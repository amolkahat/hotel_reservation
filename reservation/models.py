from django.db import models

# Create your models here.


class Hotel(models.Model):
    name = models.CharField(default='', max_length=50)
    address = models.CharField(default='', max_length=150)
    city = models.CharField(default='', max_length=30)
    capacity = models.IntegerField()

    def __str__(self):
        return self.name


class hotel_bookings(models.Model):
    hotel_name = models.CharField(max_length=50)
    user_name = models.CharField(max_length=50)
    user_stay = models.IntegerField()
    payment = models.CharField(max_length=50)

    def __str__(self):
        return self.user_name
