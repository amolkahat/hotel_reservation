from django.contrib import admin
from .models import Hotel, hotel_bookings
# Register your models here.


admin.site.register(Hotel)
admin.site.register(hotel_bookings)
