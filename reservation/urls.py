from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.hotel_register, name='hotel_register'),
    path('login/', views.user_login, name='user_login'),
    path('signup/',views.user_signup, name='user_signup'),
    path('logout/', views.user_logout, name='logout'),
    path('show_hotels/', views.show_hotels, name='show_hotels'),
    path('book_hotel/<str:hotel_name>', views.book_hotel, name='book_hotel'),
    path('profile/', views.profile, name='profile'),
]
