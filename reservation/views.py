from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .models import Hotel, hotel_bookings
# Create your views here.


def index(request):
    return render(request, 'index.html')


def hotel_register(request):
    if request.POST:
        h = Hotel(name=request.POST.get('name'),
                  address=request.POST.get('address'),
                  city=request.POST.get('city'),
                  capacity=request.POST.get('capacity'))
        h.save()
    return render(request, 'hotel_registration.html')


def user_login(request):
    context = {'error': ''}
    if request.POST:
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)

        user = authenticate(username = username, password = password)
        if user:
            login(request, user)
            return render(request, 'index.html')
        else:
            messages.error(request, 'Invalid Login Credentials!!')
    return render(request, 'login.html', context=context)


def user_signup(request):
    context = {}
    if request.POST:
        u = User()
        username = request.POST.get('username', None)
        email = request.POST.get('email', None)
        password = request.POST.get('password1', None)
        password1 = request.POST.get('password2', None)

        u.username = username
        u.email = email
        if password == password1:
            u.set_password(password)
            u.save()
            return redirect('user_login')
        else:
            messages.error(request, "Passwords do not match.",
                           extra_tags='alert alert-danger')
    return render(request, 'signup.html', context=context)


def user_logout(request):
    logout(request)
    return redirect('index')


def show_hotels(request):
    context = {}
    hotels = Hotel.objects.all()

    context['hotel_list'] = [{'name': i.name,
                              'address': i.address,
                              'city': i.city} for i in hotels]
    return render(request, 'show_hotels.html', context=context)


def book_hotel(request, hotel_name):
    context = {'hotel_name': hotel_name}
    if request.POST:
        h = hotel_bookings()
        h.name = request.POST.get('hotel_name', None)
        h.user_name = request.POST.get('user_name', None)
        h.user_stay = request.POST.get('stay', None)
        h.method = request.POST.get('method')
        h.save()
        return render(request, 'index.html',context={'msg': 'Booking success.'})

    return render(request, 'book_hotel.html', context=context)



def profile(request):

    context = {}
    context['profile'] = User.objects.get(username=request.user.username)
    return render(request, 'profile.html', context=context)
